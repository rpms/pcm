#!/bin/bash

# fail the whole test set if any of the command pipelines fail
set -ex

# when running this in 1minutetip the PATH must be specified to execute
# in the local directory.
echo "Setting path to local directory"
PATH=$PATH:$(pwd)

# simple version test
version.sh
