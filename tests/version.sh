#!/bin/bash
#
# Thermald is a systemd unit file that requires specific hardware and
# firmware.  As such it is difficult to test in an automated fashion.

# fail the whole test if any of the command pipelines below fails
set -ex

rpmversion=$(rpm -q --queryformat '%{VERSION}' pcm)
if [ "$rpmversion" == "package pcm is not installed" ]; then
	echo "pcm package not installed"
	exit 1
fi

# if we get here, it's OK

